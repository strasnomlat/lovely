﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject player;
    public GameObject pickups;

    PlayerController playerScript;
    PickupController[] pickupControllers;

    void Start()
    {
        playerScript = player.GetComponent<PlayerController>();
        pickupControllers = pickups.GetComponentsInChildren<PickupController>();
        foreach (PickupController pickupController in pickupControllers)
        {
            pickupController.setGameController(this);
            if (pickupController.type == PickupController.PickupType.Start)
            {
                pickupController.triggerPickup();
            }
        }
    }

    public void NextLevel ()
    {
        int currentIndex = SceneManager.GetActiveScene().buildIndex;
        if (currentIndex < SceneManager.sceneCountInBuildSettings - 1)
        {
            Debug.Log(currentIndex + 1);
            SceneManager.LoadScene(currentIndex + 1, LoadSceneMode.Single);
        } else
        {
            Debug.Log(0);
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
    }

    public void RestartLevel ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }

    void enablePickups (bool isEnabled)
    {
        foreach (PickupController pickupController in pickupControllers)
        {
            pickupController.isEnabled = isEnabled;
        }
    }

    void goToPlace(Vector3 target)
    {
        
    }

    public void playerReady()
    {
        enablePickups(true);
        //Debug.Log("player ready");
        //goToRandomPlace();
    }

    public void pickupSelected(PickupController target)
    {
        enablePickups(false);
        playerScript.goToPoint(target);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
