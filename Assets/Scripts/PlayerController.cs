﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public GameObject game;
    public FloatVariable speed;

    public FloatVariable fuel;
    public IntVariable keys;
    public IntVariable tracks;

    public GameObject menuPrefab;

    float inertion = 0.0f;
    bool isMoving = false;

    Vector3 targetPoint;
    Vector3 targetDirNormalized;
    PickupController targetPickup;

    GameController gameController;
    FuelSlider fuelSlider;

    GameObject pauseMenu;

    void Start()
    {
        gameController = game.GetComponent<GameController>();
        if (menuPrefab != null)
        {
            pauseMenu = Instantiate(menuPrefab);
            pauseMenu.SetActive(false);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (targetPoint != null && isMoving)
        {
            if (Vector3.Distance(targetPoint, transform.position) <= speed.Value * Time.deltaTime + 0.1f)
            {
                isMoving = false;
                claimPickup();
            }
            else
            {
                transform.position = transform.position + targetDirNormalized * speed.Value * Time.deltaTime * getEase(inertion, 4);
                addToFuel(-0.1f * speed.Value * Time.deltaTime);
                inertion = Mathf.Clamp01(inertion + 0.1f);
                if (fuel.Value == 0)
                {
                    isMoving = false;
                    gameController.RestartLevel();
                }
            }
        }

        if (pauseMenu != null && Input.GetKeyUp(KeyCode.Escape))
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf);
        }
    }

    float getEase (float t, float p)
    {
        return Mathf.Pow(t, 2) * ((p + 1) * t - p);
    }

    Vector3 createPosition(Vector3 target)
    {
        return new Vector3(target.x, transform.position.y, target.z);
    }

    void addToFuel(float value)
    {
        fuel.Value = Mathf.Clamp01(fuel.Value + value);
    }

    void claimPickup ()
    {
        if (targetPickup.type == PickupController.PickupType.Finish)
        {
            gameController.NextLevel();
        }
        else
        {
            switch (targetPickup.type)
            {
                case PickupController.PickupType.Start:
                    fuel.Value = 1.0f;
                    keys.Value = 0;
                    tracks.Value = 0;
                    break;
                case PickupController.PickupType.Fuel10:
                    addToFuel(1.0f);
                    break;
                case PickupController.PickupType.Fuel5:
                    addToFuel(0.5f);
                    break;
                case PickupController.PickupType.Key:
                    keys.Value++;
                    break;
                case PickupController.PickupType.Tracks:
                    tracks.Value++;
                    break;
            }
            targetPickup.onPickup();
            gameController.playerReady();
        }
    }

    public void goToPoint (PickupController target)
    {
        targetPoint = createPosition(target.transform.position);
        targetDirNormalized = (targetPoint - transform.position).normalized;
        targetPickup = target;

        if (targetPickup.type == PickupController.PickupType.Start)
        {
            transform.position = targetPoint;
        } else
        {
            transform.LookAt(targetPoint);
        }

        inertion = 0.0f;
        isMoving = true;

    }

    private void OnDrawGizmos()
    {
        if (targetPoint != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, targetPoint);
        }
    }
}
