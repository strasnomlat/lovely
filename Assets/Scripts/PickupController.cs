﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupController : MonoBehaviour
{
    public enum PickupType
    {
        Fuel10,
        Fuel5,
        Tracks,
        Key,
        Start,
        Finish
    }
    public PickupType type;

    public GameObject platform;
    public GameObject platformHover;

    public IntVariable keys;

    private bool isVisited = false;
    public bool isEnabled = true;

    GameController gameController;

    GameObject icon;

    ParticleSystem particles;

    private void Awake()
    {
        GameObject prefab = Resources.Load("Prefabs/Pickup" + type) as GameObject;
        if (prefab != null)
        {
            icon = Instantiate(prefab, transform);
        }
        particles = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setGameController (GameController gc)
    {
        gameController = gc;
    }

    public void onPickup()
    {
        if (icon != null)
        {
            icon.SetActive(false);
            particles.Emit(10);
        }
    }

    public bool canPickup()
    {
        return isEnabled && isValid();
    }

    public bool isValid()
    {
        if (isVisited)
        {
            return false;
        }
        if (type == PickupType.Finish && keys.Value == 0)
        {
            return false;
        }
        return true;
    }

    public void triggerPickup()
    {
        if (canPickup())
        {
            isVisited = true;
            gameController.pickupSelected(this);
            platform.SetActive(true);
            platformHover.SetActive(false);
        }
    }

    private void OnMouseUpAsButton()
    {
        triggerPickup();
    }

    private void OnMouseExit()
    {
        platform.SetActive(true);
        platformHover.SetActive(false);
    }

    private void OnMouseEnter()
    {
        if (isValid())
        {
            platform.SetActive(false);
            platformHover.SetActive(true);
        }
    }
}
