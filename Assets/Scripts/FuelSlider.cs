﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelSlider : MonoBehaviour
{

    public FloatVariable Current;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float scale = Mathf.Clamp(Current.Value, 0.01f, 1.0f);
        transform.localPosition = new Vector3(0.0f, scale - 1.0f, 0.0f);
        transform.localScale = new Vector3(1.0f, scale, 1.0f);
    }
}
